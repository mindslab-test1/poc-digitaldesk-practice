﻿using System;
using System.Diagnostics;
using System.Threading;
using System.Windows;
using System.Windows.Input;
using Newtonsoft.Json.Linq;
using RestSharp;
using RestSharp.Extensions;

namespace poc_digitaldesk_practice
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void pnlMainGrid_MouseUp(object sender, MouseButtonEventArgs e)
        {
            var client = new RestClient("http://13.125.179.153:6941/collect/run/utter");
            client.Timeout = -1;
            var request = new RestRequest(Method.POST);
            request.AddHeader("Content-Type", "application/json");
            var body = @"{" + "\n" +
                       @"    ""host"":""628""," + "\n" +
                       @"    ""session"":""digitaldesk1""," + "\n" +
                       @"    ""data"": {" + "\n" +
                       @"        ""utter"": ""마인즈랩 위치""" + "\n" +
                       @"    }," + "\n" +
                       @"    ""lang"": ""1""" + "\n" +
                       @"}";
            request.AddParameter("application/json", body, ParameterType.RequestBody);
            IRestResponse response = client.Execute(request);

            MessageBox.Show("You clicked me at " + e.GetPosition(this).ToString() + "\n" + response.Content);
        }

        private void UIElement_OnMouseUp(object sender, MouseButtonEventArgs e)
        {
            var requestKey = "";
            {
                var client = new RestClient("https://api.maum.ai/lipsync/upload");
                client.Timeout = -1;
                var request = new RestRequest(Method.POST);
                request.AddParameter("apiId", "mindslab-api-shinhan-poc");
                request.AddParameter("apiKey", "d128c5c73b8c4f14affbc66211f0afdb");
                request.AddFile("image", "C:\\Users\\jayow\\OneDrive\\사진\\maum.png");
                request.AddParameter("model", "short_lipsync_server");
                request.AddParameter("transparent", "true");
                request.AddParameter("resolution", "FHD");
                request.AddParameter("text", "내가 어제 먹다 남긴 피자 조각");
                IRestResponse response = client.Execute(request);

                JObject jObject = JObject.Parse(response.Content);
                requestKey = jObject["payload"]?["requestKey"]?.ToString() ?? throw new FormatException();
            }

            int? statusCode = null;
            while (true)
            {
                var client = new RestClient("https://api.maum.ai/lipsync/statusCheck");
                client.Timeout = -1;
                var request = new RestRequest(Method.POST);
                request.AddHeader("Content-Type", "application/json");
                var body = @"{" + "\n" +
                           @"    ""apiId"":""mindslab-api-shinhan-poc""," + "\n" +
                           @"    ""apiKey"":""d128c5c73b8c4f14affbc66211f0afdb""," + "\n" +
                           @"    ""requestKey"":""" + requestKey + @"""" + "\n" +
                           @"}";
                request.AddParameter("application/json", body, ParameterType.RequestBody);
                IRestResponse response = client.Execute(request);

                JObject jObject = JObject.Parse(response.Content);
                var statusCodeString = jObject["payload"]?["statusCode"]?.ToString() ?? throw new FormatException();
                statusCode = Int32.Parse(statusCodeString);

                if (statusCode == 2)
                    break;
                if (statusCode >= 4)
                    throw new ApplicationException();

                Thread.Sleep(200);
            }

            {
                var client = new RestClient("https://api.maum.ai/lipsync/download");
                client.Timeout = -1;
                var request = new RestRequest(Method.POST);
                request.AddHeader("Content-Type", "application/json");
                request.AddHeader("Accept", "*/*");
                request.AddHeader("Accept-Encoding", "gzip, deflate, br");
                var body = @"{" + "\n" +
                           @"    ""apiId"":""mindslab-api-shinhan-poc""," + "\n" +
                           @"    ""apiKey"":""d128c5c73b8c4f14affbc66211f0afdb""," + "\n" +
                           @"    ""requestKey"":""" + requestKey + @"""" + "\n" +
                           @"}";
                request.AddParameter("application/json", body, ParameterType.RequestBody);
                client.DownloadData(request).SaveAs("./recent.mp4");
            }
        }
    }
}